<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking the status.
 *
 * @VitalsCheck(
 *   id = "dev_modules",
 *   label = @Translation("Development modules"),
 *   description = @Translation("Returns the status for common development modules (reroute_email/symfony_mailer_reroute, shield, devel, stage_file_proxy, shield).")
 * )
 */
class DevModules extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'devel' => \Drupal::moduleHandler()->moduleExists('devel'),
      'stage_file_proxy' => FALSE,
      'shield' => FALSE,
      'reroute_email' => FALSE,
    ];

    if (\Drupal::moduleHandler()->moduleExists('stage_file_proxy') && !empty(\Drupal::config('stage_file_proxy.settings')->get('origin'))) {
      $output['stage_file_proxy'] = TRUE;
    }

    if (\Drupal::moduleHandler()->moduleExists('shield') && !empty(\Drupal::config('shield.settings')->get('shield_enable'))) {
      $output['shield'] = TRUE;
    }

    if (\Drupal::moduleHandler()->moduleExists('reroute_email')) {
      $output['reroute_email'] = \Drupal::config('reroute_email.settings')->get('enable');
    }
    elseif (\Drupal::moduleHandler()->moduleExists('symfony_mailer_reroute')) {
      $output['reroute_email'] = \Drupal::config('symfony_mailer_reroute.settings')->get('enable');
    }

    return $output;
  }

}
