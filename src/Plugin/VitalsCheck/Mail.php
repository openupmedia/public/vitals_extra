<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check for checking the mail.
 *
 * Settings of the site.
 *
 * @VitalsCheck(
 *   id = "mail",
 *   label = @Translation("Mail settings"),
 *   description = @Translation("Returns general mail settings of the website.")
 * )
 */
class Mail extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'provider' => NULL,
      'transport' => NULL,
    ];

    // Mailsystem (required by swiftmailer, optional for smtp & phpmailer_smtp)
    if (\Drupal::moduleHandler()->moduleExists('mailsystem')) {
      $mailsystem_sender = \Drupal::config('mailsystem.settings')->get('defaults.sender');
      $output['provider'] = 'mailsystem > ' . $mailsystem_sender;
      $output['transport'] = $mailsystem_sender;

      if ($mailsystem_sender == 'swiftmailer') {
        $output['transport'] = \Drupal::config('swiftmailer.transport')->get('transport');
      }
      elseif ($mailsystem_sender == 'smtp' || $mailsystem_sender == 'phpmailer_smtp') {
        $output['transport'] = 'smtp';
      }
    }
    // Symfony mailer.
    elseif (\Drupal::moduleHandler()->moduleExists('symfony_mailer')) {
      $output['provider'] = 'symfony_mailer';
      $output['transport'] = \Drupal::config('symfony_mailer.settings')->get('default_transport');
    }
    // Smtp (without mailsystem)
    elseif (\Drupal::moduleHandler()->moduleExists('smtp') && !empty(\Drupal::config('smtp.settings')->get('smtp_on'))) {
      $output['provider'] = 'smtp';
      $output['transport'] = 'smtp';
    }
    // phpmailer_smtp (without mailsystem)
    elseif (\Drupal::moduleHandler()->moduleExists('phpmailer_smtp')) {
      $output['provider'] = 'phpmailer_smtp';
      $output['transport'] = 'smtp';
    }
    // Default settings.
    else {
      $output['provider'] = \Drupal::config('system.mail')->get('interface.default');
      $output['transport'] = \Drupal::config('system.mail')->get('interface.default');
    }

    return $output;
  }

}
