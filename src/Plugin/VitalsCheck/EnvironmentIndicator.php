<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals\VitalsCheckPluginBase;

/**
 * Plugin implementation of the vitals_check.
 *
 * Checking the current environment.
 *
 * @VitalsCheck(
 *   id = "environment_indicator",
 *   label = @Translation("Environment indicator"),
 *   description = @Translation("Returns the environment name and release from in the Environment Indicator module.")
 * )
 */
class EnvironmentIndicator extends VitalsCheckPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'name' => NULL,
      'release' => NULL,
    ];

    if (\Drupal::moduleHandler()->moduleExists('environment_indicator')) {
      $output['name'] = \Drupal::config('environment_indicator.indicator')->get('name');
    }
    if (\Drupal::state()->get('environment_indicator.current_release')) {
      $output['release'] = \Drupal::state()->get('environment_indicator.current_release');
    }

    return $output;
  }

}
